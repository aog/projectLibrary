<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>PROJECT LIBRARY</title>
		%{--<script src="http://use.edgefonts.net/source-code-pro.js"></script>--}%
		%{--<g:javascript library="jquery" plugin="jquery"/>--}%

	</head>
	<body >
	<ol class="breadcrumb">
		<li><a href="${createLink(uri: '/')}" class="active">Home</a></li>
		%{--<li><a href="active">Library</a></li>--}%
	</ol>
	<div>
		<h3>MODULE/FUNCTION LISTS</h3>
		<div class="panel panel-default">
			<div class="panel-body">
				<table>
					<tr>
						<th>Function</th>
						<th>Language</th>

						<th>Description</th>
					</tr>
					<tr>
						<td><i class="icon-tags"></i> &nbsp;<g:link controller="exportPDF" action="exportPDFPage">Export PDF</g:link></td>
						<td>JQUERY</td>

						<td>export HTML to PDF</td>

					</tr>
				</table>
			</div>
		</div>
	</div>

	</body>
</html>
