<%--
  Created by IntelliJ IDEA.
  User: mananya
  Date: 5/30/16 AD
  Time: 5:02 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>EXPORT PDF</title>
    <meta name="layout" content="main"/>

    <script type="text/javascript" src="${resource(dir: 'js', file: 'jquery-1.12.4.min.js')}"></script>
    %{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>--}%

    <script type="text/javascript" src="${resource(dir: 'js', file: 'jspdf.debug.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js', file: 'html2canvas.js')}"></script>

    %{--<script type="text/javascript" src="${resource(dir: 'js', file: 'jspdf.min.js')}"></script>--}%

    <style type="text/css" media="screen"></style>
    <script>
        function genPDF(){
            html2canvas(document.getElementById('content'), {
                onrendered : function (canvas){
                    var img = canvas.toDataURL('image/png');
                    var doc = new jsPDF();
                    doc.addImage(img,'JPG',20,20);
//						margin - 20 20
                    doc.save('test.pdf')
                }
            });
        }
    </script>
    %{--<script>--}%
    %{--var doc = new jsPDF();--}%
    %{--var specialElementHandlers = {--}%
    %{--'#editor': function (element, renderer) {--}%
    %{--return true;--}%
    %{--}--}%
    %{--};--}%

    %{--$('#cmd').click(function () {--}%
    %{--console.log('exported')--}%
    %{--doc.fromHTML($('#content').html(), 15, 15, {--}%
    %{--'width': 170,--}%
    %{--'elementHandlers': specialElementHandlers--}%
    %{--});--}%
    %{--doc.save('sample-file.pdf');--}%
    %{--});--}%
    %{--</script>--}%
</head>

<body>
<ol class="breadcrumb">
    <li><a href="${createLink(uri: '/')}" >Home</a></li>
    <li><a href="#" class="active">Export PDF</a></li>
</ol>
<div id="content">
    <h3>Hello, this is a H3 tag</h3>
    <img src="${resource(dir: 'images', file: 'home.png')}"  style="width: 100px"/>&nbsp;

    <p>a pararaph</p>
    <img src="${resource(dir: 'images', file: 'home.png')}"  style="width: 100px"/>&nbsp;

</div>
<div id="editor"></div>
<button id="cmd" onclick="genPDF()">generate PDF</button>
%{--<script type="text/javascript">--}%
%{--var pdf = new jsPDF();--}%
%{--//			pdf.text(30, 30, 'Hello world!');--}%
%{--pdf.fromHTML($('#content').html(), 15, 15, {--}%
%{--'width': 170--}%
%{--//			'elementHandlers': specialElementHandlers--}%
%{--});--}%
%{--pdf.save('hello_world.pdf');--}%
%{--</script>--}%
</body>
</html>