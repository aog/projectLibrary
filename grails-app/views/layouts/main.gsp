<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>PROJECT LIBRARY</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
		<g:layoutHead/>
		<g:javascript library="application"/>		
		<r:layoutResources />
	%{-- =============== Boostrap =============--}%
	<link rel="stylesheet" href="${resource(dir: 'bootstrap-3.2.0/css', file: 'bootstrap.css')}" type="text/css">
	<script type="text/javascript" src="${resource(dir: 'bootstrap-3.2.0/js', file: 'bootstrap.min.js')}"></script>

	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="${resource(dir: 'font-awesome_3.2.1/css', file: 'font-awesome.min.css')}" type="text/css">

	<style>
	.topBand{
		padding: 20px;
		font-size: 20px;
		color:#ffffff;
	}
		.fa-camera-retro{
			color:#ffffff;
		}
</style>

</head>
	<body>
		<div id="grailsLogo" role="banner"><label class="topBand" ><i class="icon-flag"></i> &nbsp;PROJECT LIBRARY</label>
			%{--<img src="${resource(dir: 'images', file: 'grails_logo.png')}" alt="Grails"/>--}%
		</div>
	<div style="padding: 2em">
		<g:layoutBody/>
	</div>

		<div class="footer" role="contentinfo"></div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<r:layoutResources />
	</body>
</html>
