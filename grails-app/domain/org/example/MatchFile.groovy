package org.example

class MatchFile {

    static constraints = {

    }
    static mapping = {

        table"matchFile"
        version false
        // InHereName column : "INDBColumnName" ;
        id column: "file_id";
        fileName column: "fileName";
        fileOriginalName column: "fileOriginalName";

    }
    // DatatypeInDB InHereName;
    // BigInteger file_id;
    String fileName;
    String fileOriginalName;
}
