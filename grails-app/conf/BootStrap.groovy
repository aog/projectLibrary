import org.example.SecRole
import org.example.SecUser

class BootStrap {

    def init = { servletContext ->
        def adminRole = SecRole.findByAuthority('ADMIN')?:new SecRole(authority: 'ADMIN').save(failOnError: true)
        def adminUser = SecUser.findByUsername('admin') ?: new SecUser(
                username: 'admin',
                password: '1234',

                enabled: true).save(failOnError: true)

    }
    def destroy = {
    }
}
